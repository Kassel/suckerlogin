<?php

/**
 * Copyright 2012 Roman Kasl romankasl(at)gmail(dot)com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * 
 */   

?>
<!DOCTYPE html>
<html>
<head>
<title>SuckerLogin | Test page</title>
<meta name="author" content="Kassel"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<h1>SuckerLogin - Alpha 1 Scured! Unstable</h1>
<p>Please choose a server</p>
<?php

include 'SuckerLogin.php';

?>
<br />
<hr />
<br />
</body>
</html>