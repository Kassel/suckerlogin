<?php

/**
 * Copyright 2012 Roman Kasl romankasl(at)gmail(dot)com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * 
 */
 
include_once 'Providers/Facebook.php';
include_once 'Providers/Google.php';

class CSuckerLogin{
    
    private $_ERRORS = array();    
    private $_PROVIDERS = array();
    
    public function __construct(){
                        
    }
    
    public function addProvider(  AProviderOfUsers $Provider ){
        
        if($Provider->getName() == "") return false;
        
        array_push($this->_PROVIDERS, $Provider);
        
        return true;
        
    }
    
    public function writeErrors(){
        
        echo '<div class="sucker_errors">';
        foreach($this->_ERRORS as $Error){
            echo '<span>' . $Error . '</span>';
        }
        echo '</div>';
    }
    
    public function getProvidersContext(){
        
        session_start();
        session_regenerate_id();
        
        foreach($this->_PROVIDERS as $Provider){
            
            $Provider->getProviderContext();
            
        }
    }    
    
}

abstract class AProviderOfUsers{    
    
    var $ERROR;
    protected $_PROVIDER_NAME;
    protected $_TOKEN_EXPIRE;        
    
    public function getLoginUri(){}    
    public function exchangeCodeForToken($Code){}
    public function getUserData($Token){}
    public function getProviderContext(){}
    
    protected function setError($Message){ $ERROR = $Message; }
    public function getError(){ return $this->ERROR; }
    public function getName(){ return $this->_PROVIDER_NAME; }
    
}

class CUserInfo{
    private $_Name;
    private $_Id;
    private $_Link;
    private $_Picture;
    private $_Expire;
    
    public function __construct($Name, $Id, $Link, $Picture, $Expire){
        $this->_Name = $Name;
        $this->_Id = $Id;
        $this->_Link = $Link;
        $this->_Picture = $Picture;
        $this->_Expire = $Expire;
    }
    
    public function getName(){ return $this->_Name; }
    public function getId(){ return $this->_Id; }
    public function getLink(){ return $this->_Link; }
    public function getPicture(){ return $this->_Picture; }
    public function getExpire(){ return $this->_Expire; }
    
    public function writeUser(){
        echo '</br>' . "\n";
        echo '<div class="suckerlogin_userinfo">' . "\n";
        echo '<span>Name: ' . $this->_Name . '</span></br>' . "\n";
        echo '<span>Id: ' . $this->_Id . '</span></br>' . "\n";
        echo '<span>Link: ' . $this->_Link . '</span></br>' . "\n";
        //echo '<span>Expire: ' . $this->_Expire . '</span></br>' . "\n";
        echo '<img width="50" height="50" src="' . $this->_Picture . '"></img>' . "\n";
        echo '</div>' . "\n";
        
    }
    
}

?>