<?php

/**
 * Copyright 2012 Roman Kasl romankasl(at)gmail(dot)com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * 
 */   



class CSuckerLoginDatabase{
    
    private $_ERRORS;
    private $_CONNECTION;
    private $_IS_CONNECTION_ESTABLISHED;
    private $_DATABASE_NAME;    
    
    public function __construct($DatabaseHost, $DatabaseName, $UserName, $Password){
        
        $this->_DATABASE_NAME = $DatabaseName;
        $this->_IS_CONNECTION_ESTABLISHED = false;        
        
        
        @$this->_CONNECTION = mysql_connect($DatabaseHost, $UserName, $Password);
        if(!$this->_CONNECTION){
            
            if(mysql_errno() == 2002 )$this->setError("Could not connect: '" . $DatabaseHost . "' ");
            else $this->setError("Could not connect: '" . $DatabaseHost . "' " . mysql_error());
            return NULL;   
        }        
        
        if(mysql_errno($this->_CONNECTION)){
            
            $this->setError(mysql_error($this->_CONNECTION));
            mysql_close($this->_CONNECTION);
            return NULL;
        }
        
        // Select database
        $query = mysql_select_db($DatabaseName, $this->_CONNECTION);
        if(mysql_errno($this->_CONNECTION)){ 
            
            $this->setError(mysql_error($this->_CONNECTION));
            mysql_close($this->_CONNECTION);
            return NULL;
        }
        
        if(!$this->existTable('suckerlogin_providers')){
            $this->setError('Please configure database first!');
            mysql_close($this->_CONNECTION);
            return NULL;
        }        
                
        $this->_IS_CONNECTION_ESTABLISHED = true;        
    }
    
    public function __destruct(){
        if($this->_CONNECTION) mysql_close($this->_CONNECTION);   
    }
    
    public function getError(){ return $this->_ERRORS; }
    private function setError($Error){ $this->_ERRORS .= $Error . " \n"; }
    public function isEstablished(){ return $this->_IS_CONNECTION_ESTABLISHED; }
    
    private function existTable($TableName){
        
        $query = mysql_query("SHOW TABLES FROM  `suckerlogin` 
                              WHERE  `Tables_in_$this->_DATABASE_NAME` =  '$TableName';", $this->_CONNECTION);
                                      
        $result = mysql_numrows($query); 
                                          
        if(!$result) return false;        
        else return true;
    }
    
    public function existProvider($ProviderName){
        if($this->existTable("suckerlogin_" . strtolower($ProviderName) . "_provider")) return true;
        else return false;
    }
    
    public function addProvider($ProviderName){
                
        
            $query = mysql_query("CREATE TABLE IF NOT EXISTS `suckerlogin_" . strtolower($ProviderName). "_provider` (
                                  `user_id` int(11) NOT NULL,
                                  `online_token` text NOT NULL,
                                  `offline_token` text NOT NULL,
                                  `session_id` varchar(128) NOT NULL,
                                  PRIMARY KEY (`user_id`),
                                  UNIQUE KEY `session_id` (`session_id`)
                                  ) DEFAULT CHARSET=utf8;", $this->_CONNECTION);
            
            if($this->existTable("suckerlogin_" . strtolower($ProviderName) . "_provider")){
              
              mysql_query("INSERT INTO  `suckerlogin`.`suckerlogin_providers` (`id` ,`name`)
                           VALUES (NULL ,  '" . $ProviderName . "')");                           
              return true;  
            } 
            else{
                $this->setError("An error occurred during '" . $ProviderName . "' being added!");
                return false;
            }                          
        
        
    }
}

?>