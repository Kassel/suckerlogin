<?php

/**
 * Copyright 2012 Roman Kasl romankasl(at)gmail(dot)com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * 
 */   



include_once '/../SuckerLoginCore.php';

class FacebookProvider extends AProviderOfUsers{
        
    private $_endpoint = "https://www.facebook.com/dialog/oauth?";
    private $_app_id = "356749121023667";
    private $_redirect_uri = "https://suckerlogin.co.cc/";
    private $_app_secret = "df0aa53c0d0d2db4d471dc5b02869546";
    private $_state = "Facebook";
    
    public function __construct(){
        $this->_PROVIDER_NAME = "Facebook";
    }
    
    public function getLoginUri(){
        $uri = $this->_endpoint .
            "&client_id=" . $this->_app_id .
            "&redirect_uri=" . $this->_redirect_uri . 
            "&state=" . $this->_state;
        return $uri;
    }
    public function exchangeCodeForToken($Code){
        
        if(isset($_GET['error'])){
            $this->setError("Acces denied!");
            return NULL;
        }
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/oauth/access_token?client_id=" . $this->_app_id . "&redirect_uri=" . $this->_redirect_uri . "&client_secret=" . $this->_app_secret . "&code=" . $Code);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
        
        $res = curl_exec($ch);
        
        curl_close($ch);
        
        $params = explode("&", $res);
        $tokenstring  = explode("=", $params[0]);
        $expirestring = explode("=", $params[1]);
        
        
        if($tokenstring[0] != "access_token"){
          
          $this->setError("There's no access token!");
          return NULL;  
        } 
        
        if($expirestring[0] == "expires") $this->_TOKEN_EXPIRE = $expirestring[1];
        else $this->_TOKEN_EXPIRE = -1;
        
        return $tokenstring[1];
        
    }
    public function getUserData($Token){
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/me?access_token=" . $Token);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
        
        $res = curl_exec($ch);
        
        curl_close($ch);
        
        $data = json_decode($res, true);
        if(isset($_SESSION['FacebookTokenExpire'])) $this->_TOKEN_EXPIRE = $_SESSION['FacebookTokenExpire'];
        
        $userdata = new CUserInfo($data['name'], $data['id'], $data['link'], "https://graph.facebook.com/" . $data['id'] . "/picture", $this->_TOKEN_EXPIRE);
        
        return  $userdata; 
    }
    public function getProviderContext(){
        
        if(@$_GET['state'] == 'Facebook' and isset($_GET['code']) and !isset($_SESSION['FacebookToken']) ){
        
            $token = $this->exchangeCodeForToken($_GET['code']);            
            $user = $this->getUserData($token);
            $user->writeUser();
            $_SESSION['FacebookToken'] = $token;
            $_SESSION['FacebookTokenExpire'] = $this->_TOKEN_EXPIRE;                           
            
        }
        
        elseif(isset($_SESSION['FacebookToken'])){
                
            $user = $this->getUserData($_SESSION['FacebookToken']);
            $user->writeUser();
            
        
        }
        else echo "<a href=\"" . $this->getLoginUri() . "\">Facebook login</a>";
        
        $this->getError(); 
        
        echo "<br />";       
    }    
}


?>