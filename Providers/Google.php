<?php

/**
 * Copyright 2012 Roman Kasl romankasl(at)gmail(dot)com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * 
 */   


include_once '/../SuckerLoginCore.php';

class GoogleProvider extends AProviderOfUsers{    
    
    private $_endpoint = "https://accounts.google.com/o/oauth2/auth?";
    private $_scope = "https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email";
    private $_state = "Google";
    private $_redirect_uri = "https://suckerlogin.co.cc/";
    private $_response_type = "code";
    private $_client_id = "720973107641.apps.googleusercontent.com";
    private $_client_secret = "ssANwvb60vRo4tORwA6hAj5k";
    
    public function __construct(){
        $this->_PROVIDER_NAME = "Google";
    }
    
    public function getLoginUri(){
        
        $uri = $this->_endpoint . 
                "&scope=" . $this->_scope .
                "&state=" . $this->_state .
                "&redirect_uri=" . $this->_redirect_uri .
                "&response_type=" . $this->_response_type .
                "&client_id=" . $this->_client_id .
                "&access_type=offline"; // Bez offline modu servr neda odpoved pro localhost!! logick� XD ale ztr�ta skoro dne
                        
        return "$uri";
    }
    
    public function exchangeCodeForToken($Code){                        
        
        $postfields = "code=$Code&client_id=" . urlencode($this->_client_id) . "&client_secret=$this->_client_secret&redirect_uri=". urlencode($this->_redirect_uri) . "&grant_type=authorization_code";                                
        
        $ch = curl_init();
        
        // set URL and other appropriate options  
        curl_setopt($ch, CURLOPT_URL, "https://accounts.google.com/o/oauth2/token");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');            
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: accounts.google.com" , "Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);  
       
        
        // grab URL and pass it to the browser
        $res = curl_exec($ch);        
        
        // close cURL resource, and free up system resources
        curl_close($ch);
        
        $data = json_decode($res);
        
        print_r($data);
        
       return $data->access_token;
        
    }
    
    public function getUserData($Token){
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" . $Token);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
        
        $res = curl_exec($ch);
        
        curl_close($ch);
        
        $data = json_decode($res, true);
        
        $userdata = new CUserInfo($data['name'], $data['id'], $data['link'], $data['picture'], $this->_TOKEN_EXPIRE);
        
        return  $userdata;
    }
    
    public function getProviderContext(){
        
        if(isset($_GET['error'])){
            $this->setError("Acces denied!");
            return NULL;
        }
        
        if(@$_GET['state'] == 'Google' and isset($_GET['code']) and !isset($_SESSION['GoogleToken']) ){
        
            $token = $this->exchangeCodeForToken($_GET['code']);            
            $user = $this->getUserData($token);
            $user->writeUser();
            $_SESSION['GoogleToken'] = $token;    
            
        }
        
        elseif(isset($_SESSION['GoogleToken'])){
                
            $user = $this->getUserData($_SESSION['GoogleToken']);
            $user->writeUser();
        
        }
        else echo "<a href=\"" . $this->getLoginUri() . "\">Google login</a>";                
        
        $this->getError();
        
        echo "<br />";
    }
}

?>